<?php
defined('BASEPATH') or exit('No direct script access allowed');

$route['default_controller'] = 'login';
$route['404_override'] = 'error404';
$route['translate_uri_dashes'] = FALSE;

$route['admin/beranda'] = 'beranda';
$route['admin/pengurus'] = 'pengurus';
$route['admin/device'] = 'device';
$route['admin/hapus_device/(:num)'] = 'device/hapus_device/$1';

$route['admin/rfidnew'] = 'rfid/new';
$route['admin/listrfid'] = 'rfid';
$route['admin/edit_rfid/(:num)'] = 'rfid/edit/$1';
$route['admin/save_edit_rfid'] = 'rfid/save_edit_rfid';
$route['admin/hapus_rfid/(:num)'] = 'rfid/hapus_rfid/$1';

$route['admin/absensi_user'] = 'absensi';
$route['admin/last_absensi_user'] = 'absensi/last_absensi';

$route['admin/setting'] = 'setting';
$route['admin/setting/update-waktu'] = 'setting/updatewaktu';


// Route for api
$route['api'] = 'api';
$route['api/get-mode'] = 'api/getmodejson';
$route['api/add-card'] = 'api/addcardjson';
$route['api/absensi'] = 'api/absensijson';
