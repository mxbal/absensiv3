<?php

function login()
{
    if (!$_SESSION['login']) {
        redirect(base_url('login'));
    }
}
