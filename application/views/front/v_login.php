<?php

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Absensi V3</title>
    <link rel="stylesheet" href="<?=base_url();?>assets/vendors/mdi/css/materialdesignicons.min.css">
    <link rel="stylesheet" href="<?=base_url();?>assets/vendors/css/vendor.bundle.base.css">
    <link rel="stylesheet" href="<?=base_url();?>assets/css/style.css">
    <link rel="shortcut icon" href="<?=base_url();?>assets/images/favicon.png" />
  </head>
  <body>
    <div class="container-scroller">
      <div class="container-fluid page-body-wrapper full-page-wrapper">
        <div class="row w-100 m-0">
          <div class="content-wrapper full-page-wrapper d-flex align-items-center auth login-bg">
            <div class="card col-lg-4 mx-auto">
              <div class="card-body px-5 py-5">
                <h3 class="card-title text-center mb-3">Login Absensi V3</h3>
                <?php echo $this->session->flashdata('pesan')?>
                <form action="<?=base_url();?>login/check" accept-charset="utf-8" method="post">
				          <input type="hidden" name="<?=$this->security->get_csrf_token_name();?>" value="<?=$this->security->get_csrf_hash();?>">
                  <div class="form-group">
                    <label>Username *</label>
                    <input type="text" name="username" class="form-control p_input" oninvalid="this.setCustomValidity('isi dulu boss')" oninput="this.setCustomValidity('')" required>
                  </div>
                  <div class="form-group">
                    <label>Password *</label>
                    <input type="password" name="password" class="form-control p_input" oninvalid="this.setCustomValidity('isi dulu boss')" oninput="this.setCustomValidity('')" required>
                  </div>
                  <div class="form-group d-flex align-items-center justify-content-between">
                    <div class="form-check">
                      <label class="form-check-label">
                        <input type="checkbox" name="remember" value="1" class="form-check-input"> Remember me </label>
                    </div>
                    <!--<a href="#" class="forgot-pass">Forgot password</a>-->
                  </div>
                  <div class="text-center">
                    <button type="submit" class="btn btn-primary btn-block enter-btn">Login</button>
                  </div>
                </form>
                  <div class="d-flex">
                    <div class="col-md-3">
                    </div>
                    <div class="col-md-6 text-center">
                      <a href="<?=base_url();?>dashboard">
                        <button class="btn btn-youtube">
                          <i class="mdi mdi-account-card-details"></i> Dashboard </button>
                      </a>
                    </div>
                    <div class="col-md-3">
                    </div>
                  </div>
                
              </div>
            </div>
          </div>
          <!-- content-wrapper ends -->
        </div>
        <!-- row ends -->
      </div>
      <!-- page-body-wrapper ends -->
    </div>

    <script src="<?=base_url();?>assets/vendors/js/vendor.bundle.base.js"></script>
    <script src="<?=base_url();?>assets/js/off-canvas.js"></script>
    <script src="<?=base_url();?>assets/js/hoverable-collapse.js"></script>
    <script src="<?=base_url();?>assets/js/misc.js"></script>
    <script src="<?=base_url();?>assets/js/settings.js"></script>
    <script src="<?=base_url();?>assets/js/todolist.js"></script>
  </body>
</html>