<?php
if (isset($flag)) {
  if ($flag == "pilih-device") {
?>
    <!DOCTYPE html>
    <html lang="en">
      <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>Absensi V3</title>
        <link rel="stylesheet" href="<?=base_url();?>assets/vendors/mdi/css/materialdesignicons.min.css">
        <link rel="stylesheet" href="<?=base_url();?>assets/vendors/css/vendor.bundle.base.css">
        <link rel="stylesheet" href="<?=base_url();?>assets/css/style.css">
        <link rel="shortcut icon" href="<?=base_url();?>assets/images/favicon.png" />
      </head>
      <body>
        <div class="container-scroller">
          <div class="container-fluid page-body-wrapper full-page-wrapper">
            <div class="row w-100 m-0">
              <div class="content-wrapper full-page-wrapper d-flex align-items-center auth login-bg">
                <div class="card col-lg-8 mx-auto">
                  <div class="card-body px-5 py-5">
                    <h3 class="card-title text-center mb-3">Pilih Dashboard Device</h3>
                    <?php
                      if (isset($device)) {
                        foreach ($device as $key => $value) {
                    ?>
                        <h4 class="card-title text-center mb-3">
                        <a href="<?=base_url();?>dashboard/device/<?=$value->id_device;?>">
                        <div class="d-flex" style="padding-top:30px;">
                          <div class="col-md-12 text-center">
                            Device <?=$value->id_device;?><br>
                            <?=$value->nama_device;?>
                          </div>
                        </div>
                        </a>
                        </h4>
                    <?php
                        }
                      }
                    ?>

                    <div class="d-flex" style="padding-top:50px;">
                      <div class="col-md-3">
                      </div>
                      <div class="col-md-6 text-center">
                        <a href="<?=base_url();?>">
                          <button class="btn btn-facebook">
                            <i class="mdi mdi-camera-front-variant"></i> Login Admin </button>
                        </a>
                      </div>
                      <div class="col-md-3">
                      </div>
                    </div>
                  </div>

                </div>
              </div>
              <!-- content-wrapper ends -->
            </div>
            <!-- row ends -->
          </div>
          <!-- page-body-wrapper ends -->
        </div>

        <script src="<?=base_url();?>assets/vendors/js/vendor.bundle.base.js"></script>
        <script src="<?=base_url();?>assets/js/off-canvas.js"></script>
        <script src="<?=base_url();?>assets/js/hoverable-collapse.js"></script>
        <script src="<?=base_url();?>assets/js/misc.js"></script>
        <script src="<?=base_url();?>assets/js/settings.js"></script>
        <script src="<?=base_url();?>assets/js/todolist.js"></script>
      </body>
    </html>

<?php
  }else{
    $id = 0;
    $nama = "";
    if (isset($device)) {
      foreach ($device as $key => $value) {
        $id = $value->id_device;
        $nama = $value->nama_device;
      }
    }
?>
    <!DOCTYPE html>
    <html lang="en">
      <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>Absensi V3</title>
        <link rel="stylesheet" href="<?=base_url();?>assets/vendors/mdi/css/materialdesignicons.min.css">
        <link rel="stylesheet" href="<?=base_url();?>assets/vendors/css/vendor.bundle.base.css">
        <link rel="stylesheet" href="<?=base_url();?>assets/css/style.css">
        <link rel="shortcut icon" href="<?=base_url();?>assets/images/favicon.png" />
      </head>
      <body>
        <div class="container-scroller">
          <div class="container-fluid page-body-wrapper full-page-wrapper">
            <div class="row w-100 m-0">
              <div class="content-wrapper full-page-wrapper d-flex align-items-center auth login-bg">
                <div class="card col-lg-8 mx-auto">
                  <div class="card-body px-5 py-5">
                    <h3 class="card-title text-center mb-3">Dashboard Absensi Device <?=$id;?><br><?=$nama;?></h3>
                      <div class="d-flex" style="padding-top:30px;">
                        <div class="col-md-12 text-center">
                          <h4><i>Absensi Masuk</i></h4>
                        </div>
                      </div>
                      <div class="d-flex" style="padding-top:30px;">
                        <div class="col-md-6 text-right">
                          Nama : ???<br>
                          UID RFID : ???<br>
                          Gender : ???<br>
                          Jabatan : ???<br>
                          Alamat : ???<br>
                          Waktu : ???<br>
                          Keterangan : ???
                        </div>
                        <div class="col-md-6">
                          <img src="<?=base_url();?>assets/images/wait/4.png">
                        </div>
                      </div>
                      <div class="d-flex" style="padding-top:50px;">
                        <div class="col-md-3">
                        </div>
                        <div class="col-md-6 text-center">
                          <a href="<?=base_url();?>">
                            <button class="btn btn-facebook">
                              <i class="mdi mdi-camera-front-variant"></i> Login Admin </button>
                          </a>
                        </div>
                        <div class="col-md-3">
                        </div>
                      </div>
                    
                  </div>
                </div>
              </div>
              <!-- content-wrapper ends -->
            </div>
            <!-- row ends -->
          </div>
          <!-- page-body-wrapper ends -->
        </div>

        <script src="<?=base_url();?>assets/vendors/js/vendor.bundle.base.js"></script>
        <script src="<?=base_url();?>assets/js/off-canvas.js"></script>
        <script src="<?=base_url();?>assets/js/hoverable-collapse.js"></script>
        <script src="<?=base_url();?>assets/js/misc.js"></script>
        <script src="<?=base_url();?>assets/js/settings.js"></script>
        <script src="<?=base_url();?>assets/js/todolist.js"></script>
      </body>
    </html>
<?php
  }
}
?>
