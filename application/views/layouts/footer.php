</div>
</div>
<!-- container-scroller -->
<!-- plugins:js -->
<script src="<?= base_url(); ?>assets/vendors/js/vendor.bundle.base.js"></script>
<!-- endinject -->
<!-- inject:js -->
<script src="<?= base_url(); ?>assets/js/off-canvas.js"></script>
<script src="<?= base_url(); ?>assets/js/hoverable-collapse.js"></script>
<script src="<?= base_url(); ?>assets/js/misc.js"></script>
<script src="<?= base_url(); ?>assets/js/settings.js"></script>
<script src="<?= base_url(); ?>assets/js/todolist.js"></script>
<!-- endinject -->
<!-- Custom js for this page -->
<script src="<?= base_url(); ?>assets/js/dashboard.js"></script>
<!-- End custom js for this page -->
<!-- DataTables -->
<script src="<?= base_url(); ?>assets/vendors/jquery/jquery-3.5.1.js"></script>
<script src="<?= base_url(); ?>assets/vendors/datatables/jquery.dataTables.min.js"></script>
<script src="<?= base_url(); ?>assets/vendors/datatables/dataTables-bs4.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.3.0/js/dataTables.responsive.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.3.0/js/responsive.bootstrap4.min.js"></script>
<script src="<?= base_url(); ?>assets/vendors/izitoast/izitoast.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('.datatable').DataTable();
        $('#tanggal').daterangepicker()
    });
</script>

<?php if ($this->session->flashdata('success')) : ?>
    <script>
        iziToast.success({
            title: 'Selamat',
            message: '<?= $this->session->flashdata("success") ?>',
            position: 'topRight'
        });
    </script>
<?php endif; ?>

<?php if ($this->session->flashdata('error')) : ?>
    <script>
        iziToast.error({
            title: 'Error!',
            message: '<?= $this->session->flashdata("error") ?>',
            position: 'topRight'
        });
    </script>
<?php endif; ?>
</body>

</html>