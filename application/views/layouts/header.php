<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Admin - <?= $title; ?></title>
    <!-- plugins:css -->
    <link rel="stylesheet" href="<?= base_url(); ?>assets/vendors/mdi/css/materialdesignicons.min.css">
    <link rel="stylesheet" href="<?= base_url(); ?>assets/vendors/css/vendor.bundle.base.css">
    <!-- DataTables -->
    <link rel="stylesheet" href="<?= base_url(); ?>assets/vendors/datatables/dataTables-bs4.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.3.0/css/responsive.bootstrap4.min.css">
    <link rel="stylesheet" href="<?= base_url(); ?>assets/vendors/izitoast/izitoast.css">
    <!-- Layout styles -->
    <link rel="stylesheet" href="<?= base_url(); ?>assets/css/style.css">
    <!-- End layout styles -->
    <link rel="shortcut icon" href="<?= base_url(); ?>assets/images/favicon.png" />
</head>

<body>
    <div class="container-scroller">
        <?php $this->load->view('admin/side_menu.php'); ?>
        <div class="container-fluid page-body-wrapper">
            <?php $this->load->view('admin/nav_bar.php'); ?>