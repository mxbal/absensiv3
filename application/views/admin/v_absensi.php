<?php
$title = "";
if ($flag == "absensi") {
  $title = "ABSENSI";
} else if ($flag == "last-absensi") {
  $title = "ABSENSI";
}

$this->load->view('layouts/header.php', ['title' => $title]);

?>

<div class="main-panel">
  <div class="content-wrapper">
    <?php if ($this->session->flashdata('pesan') != "") {
    ?>
      <div class="row text-center">
        <div class="col-12 grid-margin stretch-card">
          <div class="card corona-gradient-card">
            <div class="card-body py-0 px-0 px-sm-3">
              <div class="row align-items-center">

                <div class="col-4 col-sm-3 col-xl-2">
                  <img src="<?= base_url(); ?>assets/images/dashboard/circle.svg" class="gradient-corona-img img-fluid" alt="">
                </div>
                <div class="col-5 col-sm-7 col-xl-8 p-0">
                  <?php echo $this->session->flashdata('pesan') ?>
                </div>
                <div class="col-3 col-sm-2 col-xl-2 pl-0 text-center">
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    <?php
    }

    if ($flag == "absensi") {
    ?>
      <div class="row ">
        <div class="col-lg-12 grid-margin stretch-card">
          <div class="card">
            <div class="card-body">
              <h4 class="card-title">Daftar ABSENSI</h4>

              <form action="" method="get">
                <div class="form-group row">
                  <div class="col-md-4">
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i class="mdi mdi-calendar"></i></span>
                      </div>
                      <input type="date" name="from" autocomplete="off" class="form-control pull-right" id="from" value="<?= $this->input->get('from') ?>">
                    </div>
                  </div>

                  <div class="col-md-4">
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i class="mdi mdi-calendar"></i></span>
                      </div>
                      <input type="date" name="to" autocomplete="off" class="form-control pull-right" id="to" value="<?= $this->input->get('to') ?>">
                    </div>
                  </div>

                  <div class="col-md-4">
                    <button type="submit" class="btn btn-danger">Submit</button>
                    <a href="<?= base_url('absensi/export') ?>?from=<?= $this->input->get('from') ?>&to=<?= $this->input->get('to') ?>" class="btn btn-success"> Export</a>
                  </div>
                </div>
              </form>

              <div class="table-responsive">
                <table class="table table-dark datatable dt-responsive nowrap" style="width:100%">
                  <thead>
                    <tr>
                      <th> No </th>
                      <th> Nama Device </th>
                      <th> UID RFID </th>
                      <th> Nama </th>
                      <th> Jabatan </th>
                      <th> Waktu Masuk </th>
                      <th> Foto Masuk </th>
                      <th> Waktu Keluar </th>
                      <th> Foto keluar </th>
                      <th> Keterangan </th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php
                    if (isset($absensi)) {
                      $no = 0;
                      foreach ($absensi as $key => $value) {
                        if ($value->nama != "") {
                          $no++;
                    ?>
                          <tr>
                            <td style="background:#212529;text-align:center;" <?php if (fmod($no, 2) == 0) echo 'class="text-danger"';
                                                                              else echo 'class="text-success"'; ?>>
                              <?= $no; ?>
                            </td>
                            <td style="background:#212529" <?php if (fmod($no, 2) == 0) echo 'class="text-danger"';
                                                            else echo 'class="text-success"'; ?>> <?= $value->nama_device; ?> </td>
                            <td style="background:#212529" <?php if (fmod($no, 2) == 0) echo 'class="text-danger"';
                                                            else echo 'class="text-success"'; ?>> <?= $value->uid; ?> </td>
                            <td style="background:#212529" <?php if (fmod($no, 2) == 0) echo 'class="text-danger"';
                                                            else echo 'class="text-success"'; ?>> <?= $value->nama; ?> </td>
                            <td style="background:#212529" <?php if (fmod($no, 2) == 0) echo 'class="text-danger"';
                                                            else echo 'class="text-success"'; ?>> <?= $value->jabatan; ?> </td>
                            <td style="background:#212529" <?php if (fmod($no, 2) == 0) echo 'class="text-danger"';
                                                            else echo 'class="text-success"'; ?>> <?= date("d/m/Y H:i:s", $value->waktu_masuk); ?> </td>
                            <td style="background:#212529">
                              <img src="<?= base_url(); ?>assets/images/absensi/<?= $value->foto_masuk; ?>">
                            </td>
                            <td style="background:#212529" <?php if (fmod($no, 2) == 0) echo 'class="text-danger"';
                                                            else echo 'class="text-success"'; ?>> <?php if ($value->waktu_keluar > 0) echo date("d/m/Y H:i:s", $value->waktu_keluar); ?> </td>
                            <td style="background:#212529">
                              <?php if ($value->waktu_keluar > 0) { ?>
                                <img src="<?= base_url(); ?>assets/images/absensi/<?= $value->foto_keluar; ?>">
                              <?php } ?>
                            </td>
                            <td style="background:#212529" <?php if (fmod($no, 2) == 0) echo 'class="text-danger"';
                                                            else echo 'class="text-success"'; ?>> <?= $value->keterangan; ?> </td>
                          </tr>
                    <?php
                        }
                      }
                    }
                    ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>

    <?php
    } else if ($flag == "last-absensi") {
      $tgl = "";
      if (isset($waktuabsensi)) {
        $tgl = $waktuabsensi;
      }
    ?>
      <div class="row ">
        <div class="col-lg-12 grid-margin stretch-card">
          <div class="card">
            <div class="card-body">
              <h4 class="card-title">Daftar ABSENSI <?= $tgl; ?></h4>

              <form action="<?= base_url(); ?>admin/last_absensi_user" method="post">
                <input type="hidden" name="<?= $this->security->get_csrf_token_name(); ?>" value="<?= $this->security->get_csrf_hash(); ?>">
                <div class="form-group">
                  <div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text"><i class="mdi mdi-calendar"></i></span>
                    </div>
                    <input type="text" name="tanggal" autocomplete="off" class="form-control pull-right" id="tanggal">
                    <button type="submit" class="btn btn-danger">Ambil Data Absensi</button>
                  </div>
                  <!-- /.input group -->
                </div>
              </form>

              <div class="table-responsive">
                <table class="table table-dark" id="t1" style="width:100%">
                  <thead>
                    <tr>
                      <th> No </th>
                      <th> Nama Device </th>
                      <th> UID RFID </th>
                      <th> Nama </th>
                      <th> Jabatan </th>
                      <th> Waktu Masuk </th>
                      <th> Foto Masuk </th>
                      <th> Waktu Keluar </th>
                      <th> Foto keluar </th>
                      <th> Keterangan </th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php
                    if (isset($absensi)) {
                      $no = 0;
                      foreach ($absensi as $key => $value) {
                        if ($value->nama != "") {
                          $no++;
                    ?>
                          <tr>
                            <td style="background:#212529;text-align:center;" <?php if (fmod($no, 2) == 0) echo 'class="text-danger"';
                                                                              else echo 'class="text-success"'; ?>>
                              <?= $no; ?>
                            </td>
                            <td style="background:#212529" <?php if (fmod($no, 2) == 0) echo 'class="text-danger"';
                                                            else echo 'class="text-success"'; ?>> <?= $value->nama_device; ?> </td>
                            <td style="background:#212529" <?php if (fmod($no, 2) == 0) echo 'class="text-danger"';
                                                            else echo 'class="text-success"'; ?>> <?= $value->uid; ?> </td>
                            <td style="background:#212529" <?php if (fmod($no, 2) == 0) echo 'class="text-danger"';
                                                            else echo 'class="text-success"'; ?>> <?= $value->nama; ?> </td>
                            <td style="background:#212529" <?php if (fmod($no, 2) == 0) echo 'class="text-danger"';
                                                            else echo 'class="text-success"'; ?>> <?= $value->jabatan; ?> </td>
                            <td style="background:#212529" <?php if (fmod($no, 2) == 0) echo 'class="text-danger"';
                                                            else echo 'class="text-success"'; ?>> <?= date("H:i:s d M Y", $value->waktu_masuk); ?> </td>
                            <td style="background:#212529">
                              <img src="<?= base_url(); ?>assets/images/absensi/<?= $value->foto_masuk; ?>">
                            </td>
                            <td style="background:#212529" <?php if (fmod($no, 2) == 0) echo 'class="text-danger"';
                                                            else echo 'class="text-success"'; ?>> <?php if ($value->waktu_keluar > 0) echo date("H:i:s d M Y", $value->waktu_keluar); ?> </td>
                            <td style="background:#212529">
                              <?php if ($value->waktu_keluar > 0) { ?>
                                <img src="<?= base_url(); ?>assets/images/absensi/<?= $value->foto_keluar; ?>">
                              <?php } ?>
                            </td>
                            <td style="background:#212529" <?php if (fmod($no, 2) == 0) echo 'class="text-danger"';
                                                            else echo 'class="text-success"'; ?>> <?= $value->keterangan; ?> </td>
                          </tr>
                    <?php
                        }
                      }
                    }
                    ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    <?php
    }
    ?>
  </div>
  <?php
  $this->load->view('admin/footer');
  ?>

</div>

<?php $this->load->view('layouts/footer.php') ?>