<?php $this->load->view('layouts/header.php', ['title' => $title]); ?>

<div class="main-panel">
	<div class="content-wrapper">
		<?php if ($this->session->flashdata('pesan') != "") {
		?>
			<div class="row text-center">
				<div class="col-12 grid-margin stretch-card">
					<div class="card corona-gradient-card">
						<div class="card-body py-0 px-0 px-sm-3">
							<div class="row align-items-center">

								<div class="col-4 col-sm-3 col-xl-2">
									<img src="<?= base_url(); ?>assets/images/dashboard/circle.svg" class="gradient-corona-img img-fluid" alt="">
								</div>
								<div class="col-5 col-sm-7 col-xl-8 p-0">
									<?php echo $this->session->flashdata('pesan') ?>
								</div>
								<div class="col-3 col-sm-2 col-xl-2 pl-0 text-center">
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		<?php } ?>

		<div class="row ">
			<div class="col-lg-12 grid-margin stretch-card">
				<div class="card">
					<div class="card-body">
						<h4 class="card-title">History Device</h4>

						<div class="table-responsive">
							<table class="table table-dark datatable dt-responsive nowrap" style="width:100%">
								<thead>
									<tr>
										<th> No </th>
										<th> Tanggal </th>
										<th> Device </th>
										<th> Keterangan </th>
									</tr>
								</thead>
								<tbody>
									<?php
									$no = 1;
									foreach ($histories as $history) :
									?>
										<tr>
											<td><?= $no++ ?></td>
											<td><?= date('d/m/Y H:i:s', strtotime($history->waktu)) ?></td>
											<td><?= $history->nama_device ?> (<?= $history->id_device ?>)</td>
											<td><?= $history->ket ?></td>
										</tr>
									<?php endforeach; ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>

	</div>
	<?php $this->load->view('admin/footer'); ?>
</div>

<?php $this->load->view('layouts/footer.php') ?>