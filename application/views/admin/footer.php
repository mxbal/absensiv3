<footer class="footer">
	<div class="d-sm-flex justify-content-center justify-content-sm-between">
	  <span class="text-muted text-center text-sm-left d-block d-sm-inline-block">Copyright © 2021 <a href="https://tytomulyono.com/" target="_blank">Tyto Mulyono</a>. All rights reserved.</span>
	  <span class="text-muted float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Version 3.0.0</span>
	</div>
</footer>