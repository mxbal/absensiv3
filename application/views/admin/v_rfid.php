<?php
$title = "";
if ($flag == "rfid") {
  $title = "RFID";
} else if ($flag == "new") {
  $title = "RFID Baru";
} else if ($flag == "new") {
  $title = "Edit RFID";
}

$this->load->view('layouts/header.php', ['title' => $title]);
?>

<!-- Content -->
<div class="main-panel">
  <div class="content-wrapper">
    <?php if ($this->session->flashdata('pesan') != "") {
    ?>
      <div class="row text-center">
        <div class="col-12 grid-margin stretch-card">
          <div class="card corona-gradient-card">
            <div class="card-body py-0 px-0 px-sm-3">
              <div class="row align-items-center">

                <div class="col-4 col-sm-3 col-xl-2">
                  <img src="<?= base_url(); ?>assets/images/dashboard/circle.svg" class="gradient-corona-img img-fluid" alt="">
                </div>
                <div class="col-5 col-sm-7 col-xl-8 p-0">
                  <?php echo $this->session->flashdata('pesan') ?>
                </div>
                <div class="col-3 col-sm-2 col-xl-2 pl-0 text-center">
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    <?php
    }

    if ($flag == "rfid") {
    ?>

      <div class="row">
        <div class="col-lg-12 grid-margin stretch-card">
          <div class="card">
            <div class="card-body">
              <h4 class="card-title">Daftar RFID</h4>
              <div class="table-responsive">
                <table class="table table-dark datatable" style="width:100%">
                  <thead>
                    <tr>
                      <th> No </th>
                      <th> UID RFID </th>
                      <th> Nama </th>
                      <th> Telp </th>
                      <th> Gender </th>
                      <th> Jabatan </th>
                      <th> Alamat </th>
                      <th> # </th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php
                    if (isset($rfid)) {
                      $no = 0;
                      foreach ($rfid as $key => $value) {
                        if ($value->nama != "") {
                          $no++;
                    ?>
                          <tr>
                            <td style="background:#212529;text-align:center;" <?php if (fmod($no, 2) == 0) echo 'class="text-danger"';
                                                                              else echo 'class="text-success"'; ?>>
                              <?= $no; ?>
                            </td>
                            <td style="background:#212529" <?php if (fmod($no, 2) == 0) echo 'class="text-danger"';
                                                            else echo 'class="text-success"'; ?>> <?= $value->uid; ?> </td>
                            <td style="background:#212529" <?php if (fmod($no, 2) == 0) echo 'class="text-danger"';
                                                            else echo 'class="text-success"'; ?>> <?= $value->nama; ?> </td>
                            <td style="background:#212529" <?php if (fmod($no, 2) == 0) echo 'class="text-danger"';
                                                            else echo 'class="text-success"'; ?>> <?= $value->telp; ?> </td>
                            <td style="background:#212529" <?php if (fmod($no, 2) == 0) echo 'class="text-danger"';
                                                            else echo 'class="text-success"'; ?>> <?= $value->gender; ?> </td>
                            <td style="background:#212529" <?php if (fmod($no, 2) == 0) echo 'class="text-danger"';
                                                            else echo 'class="text-success"'; ?>> <?= $value->jabatan; ?> </td>
                            <td style="background:#212529" <?php if (fmod($no, 2) == 0) echo 'class="text-danger"';
                                                            else echo 'class="text-success"'; ?>> <?= $value->alamat; ?> </td>
                            <td style="background:#212529">
                              <a href="<?= base_url(); ?>admin/edit_rfid/<?= $value->id_rfid; ?>" title="ubah"><button class="btn btn-success"><i class="mdi mdi-pencil"></i></button></a>
                              <a href="<?= base_url(); ?>admin/hapus_rfid/<?= $value->id_rfid; ?>" title="hapus" onclick="return confirm('Anda Yakin menghapus data ini?')"><button class="btn btn-danger"><i class="mdi mdi-delete"></i></button></a>
                            </td>
                          </tr>
                    <?php
                        }
                      }
                    }
                    ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>

    <?php
    } else if ($flag == "new") {
    ?>
      <div class="row ">
        <div class="col-lg-12 grid-margin stretch-card">
          <div class="card">
            <div class="card-body">
              <h4 class="card-title">Daftar Kartu RFID Baru</h4>
              <div class="table-responsive">
                <table class="table table-dark datatable" style="width:100%">
                  <thead>
                    <tr>
                      <th> No </th>
                      <th> UID RFID </th>
                      <th> # </th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php
                    if (isset($rfid)) {
                      $no = 0;
                      foreach ($rfid as $key => $value) {
                        if ($value->nama == "") {
                          $no++;
                    ?>
                          <tr>
                            <td style="background:#212529" <?php if (fmod($no, 2) == 0) echo 'class="text-danger"';
                                                            else echo 'class="text-success"'; ?>>
                              <?= $no; ?>
                            </td>
                            <td style="background:#212529" <?php if (fmod($no, 2) == 0) echo 'class="text-danger"';
                                                            else echo 'class="text-success"'; ?>> <?= $value->uid; ?> </td>
                            <td style="background:#212529">
                              <a href="<?= base_url(); ?>admin/edit_rfid/<?= $value->id_rfid; ?>" title="ubah"><button class="btn btn-success"><i class="mdi mdi-pencil"></i></button></a>
                              <a href="<?= base_url(); ?>admin/hapus_rfid/<?= $value->id_rfid; ?>" title="hapus" onclick="return confirm('Anda Yakin menghapus data ini?')"><button class="btn btn-danger"><i class="mdi mdi-delete"></i></button></a>
                            </td>
                          </tr>
                        <?php
                        }
                      }
                      if ($no == 0) {
                        ?>
                        <tr>
                          <td style="background:#212529" colspan="3">Tidak ada data</td>
                        </tr>
                    <?php
                      }
                    }
                    ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    <?php
    } else if ($flag == "edit") {
      $id_rfid = 0;
      $uid = "";
      $nama = "";
      $gender = "";
      $telp = "";
      $alamat = "";
      $jabatan = "";
      if (isset($rfid)) {
        foreach ($rfid as $key => $value) {
          $id_rfid = $value->id_rfid;
          $uid = $value->uid;
          $nama = $value->nama;
          $gender = $value->gender;
          $telp = $value->telp;
          $alamat = $value->alamat;
          $jabatan = $value->jabatan;
        }
      }
    ?>
      <div class="row ">
        <div class="col-lg-12 grid-margin stretch-card">
          <div class="card">
            <div class="card-body">
              <h4 class="card-title">Edit RFID</h4>
              <p class="card-description"> UID <?= $uid; ?> </p>
              <form class="forms-sample" accept-charset="utf-8" method="post" action="<?= base_url(); ?>admin/save_edit_rfid">
                <input type="hidden" name="<?= $this->security->get_csrf_token_name(); ?>" value="<?= $this->security->get_csrf_hash(); ?>">
                <input type="hidden" name="id_rfid" value="<?= $id_rfid; ?>">
                <div class="form-group row">
                  <label class="col-sm-3 col-form-label">Nama</label>
                  <div class="col-sm-9">
                    <input type="text" name="nama" class="form-control" placeholder="nama" value="<?= $nama; ?>">
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-sm-3 col-form-label">Telp</label>
                  <div class="col-sm-9">
                    <input type="text" name="telp" class="form-control" placeholder="telp" value="<?= $telp; ?>">
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-sm-3 col-form-label">Gender</label>
                  <div class="col-sm-9">
                    <input type="text" name="gender" class="form-control" placeholder="gender" value="<?= $gender; ?>">
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-sm-3 col-form-label">Jabatan</label>
                  <div class="col-sm-9">
                    <input type="text" name="jabatan" class="form-control" placeholder="jabatan" value="<?= $jabatan; ?>">
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-sm-3 col-form-label">Alamat</label>
                  <div class="col-sm-9">
                    <input type="text" name="alamat" class="form-control" placeholder="alamat" value="<?= $alamat; ?>">
                  </div>
                </div>

                <button type="submit" class="btn btn-primary mr-2">Simpan</button>
              </form>
            </div>
          </div>
        </div>
      </div>
    <?php
    }
    ?>
  </div>
  <?php
  $this->load->view('admin/footer');
  ?>

</div>

<?php $this->load->view('layouts/footer.php'); ?>