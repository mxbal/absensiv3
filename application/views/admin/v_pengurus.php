<?php $this->load->view('layouts/header.php', ['title' => 'Pengurus']) ?>

<div class="main-panel">
	<div class="content-wrapper">
		<?php if ($this->session->flashdata('pesan') != "") {
		?>
			<div class="row text-center">
				<div class="col-12 grid-margin stretch-card">
					<div class="card corona-gradient-card">
						<div class="card-body py-0 px-0 px-sm-3">
							<div class="row align-items-center">

								<div class="col-4 col-sm-3 col-xl-2">
									<img src="<?= base_url(); ?>assets/images/dashboard/circle.svg" class="gradient-corona-img img-fluid" alt="">
								</div>
								<div class="col-5 col-sm-7 col-xl-8 p-0">
									<?php echo $this->session->flashdata('pesan') ?>
								</div>
								<div class="col-3 col-sm-2 col-xl-2 pl-0 text-center">
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		<?php
		}
		?>

		<div class="row ">
			<div class="col-lg-12 grid-margin stretch-card">
				<div class="card">
					<div class="card-body">
						<h4 class="card-title">Pengurus Sistem</h4>
						<p class="card-description">
							<button type="button" class="btn btn-primary btn-tambah" data-toggle="modal" data-target="#modalpengurus">
								Tambah Pengurus
							</button>
						</p>
						<div class="table-responsive">
							<table class="table table-dark table-striped datatable">
								<thead>
									<tr>
										<th> No </th>
										<th> Pengurus </th>
										<th> Nama </th>
										<th> Username </th>
										<th> Status </th>
										<th> # </th>
									</tr>
								</thead>
								<tbody>
									<?php
									if (isset($pengurus)) {
										$no = 0;
										foreach ($pengurus as $key => $value) {
											$no++;
									?>
											<tr>
												<td class="py-1">
													<?= $no; ?>
												</td>
												<td class="py-1">
													<img src="<?= base_url(); ?>assets/images/admin/<?= $value->avatar; ?>" alt="image" />
												</td>
												<td> <?= $value->nama; ?> </td>
												<td> <?= $value->username; ?> </td>
												<td>
													<?php
													if ($value->status == 0) {
														echo '<div class="badge badge-outline-danger">Disable</div>';
													} else {
														echo '<div class="badge badge-outline-success">Aktif</div>';
													}
													?>
												</td>
												<td>
													<button class="btn btn-success btn-edit" data-toggle="modal" data-target="#modalpengurus" id="<?= $value->id_admin ?>"><i class="mdi mdi-pencil"></i></button>
													<button class="btn btn-danger btn-delete" onclick="return confirm('Hapus data pengurus ?')" id="<?= $value->id_admin ?>"><i class="mdi mdi-delete"></i></button>
												</td>
											</tr>
									<?php
										}
									}
									?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>


		<div class="modal fade" id="modalpengurus" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="exampleModalLabel">Form Data Pengurus</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body">
						<div class="form-group">
							<label for="username">Username</label>
							<input type="text" name="username" id="username" class="form-control">

							<small class="text-danger" id="error-username"></small>
						</div>

						<div class="form-group">
							<label for="nama">Nama</label>
							<input type="text" name="nama" id="nama" class="form-control">

							<small class="text-danger" id="error-nama"></small>
						</div>

						<div class="form-group">
							<label for="password">Password</label>
							<input type="password" name="password" id="password" class="form-control">

							<small class="text-danger" id="error-password"></small>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
						<button type="button" class="btn btn-primary btn-save">Save</button>
					</div>

				</div>
			</div>
		</div>
	</div>

	<?php
	$this->load->view('admin/footer');
	?>

</div>

<?php $this->load->view('layouts/footer.php') ?>

<script>
	$(document).ready(function() {
		var url = '';

		$(".btn-tambah").on('click', function() {
			url = '<?= base_url('pengurus/store') ?>';
		});

		$(".datatable").on('click', '.btn-edit', function() {
			let id = $(this).attr('id');

			url = '<?= base_url('pengurus/update/') ?>' + id;


			$.ajax({
				url: '<?= base_url('pengurus/find/') ?>' + id,
				type: 'GET',
				success: function(res) {
					let pengurus = JSON.parse(res);

					$("#username").val(pengurus.username)
					$("#nama").val(pengurus.nama)
					$("#password").val()
				}
			})
		});

		$(".btn-save").on('click', function() {
			let username = $("#username").val()
			let nama = $("#nama").val()
			let password = $("#password").val()

			data = {
				username: username,
				nama: nama,
				password: password,
			}

			$.ajax({
				url: url,
				type: 'POST',
				data: data,
				success: function(res) {
					let response = JSON.parse(res)

					if (response.status == 'failed') {
						$("#error-username").empty().append(response.username)
						$("#error-nama").empty().append(response.nama)
						$("#error-password").empty().append(response.password)
					} else {
						$("[data-dismiss=modal]").trigger({
							type: "click"
						});

						iziToast.success({
							title: 'Selamat',
							message: response.message,
							position: 'topRight'
						});

						setTimeout(function() {
							location.reload();
						}, 3000)
					}
				}
			})
		})

		$(".datatable").on('click', '.btn-delete', function() {
			let id = $(this).attr('id');
			$.ajax({
				url: '<?= base_url('pengurus/destroy/') ?>' + id,
				type: 'POST',
				success: function(res) {
					let response = JSON.parse(res)

					iziToast.success({
						title: 'Selamat',
						message: response.message,
						position: 'topRight'
					});

					setTimeout(function() {
						location.reload();
					}, 3000)
				}
			})
		})
	});
</script>