<nav class="sidebar sidebar-offcanvas" id="sidebar">
	<div class="sidebar-brand-wrapper d-none d-lg-flex align-items-center justify-content-center fixed-top">
		<h3 style="color:#fff" class="sidebar-brand brand-logo text-center">Absensi RFID CAM</h3>
		<h3 style="color:#fff" class="sidebar-brand brand-logo-mini text-center">V3</h3>
	</div>
	<ul class="nav">
		<li class="nav-item profile">
			<div class="profile-desc">
				<div class="profile-pic">
					<div class="count-indicator">
						<img class="img-xs rounded-circle " src="<?= base_url(); ?>assets/images/admin/<?= $this->session->userdata('avatar'); ?>" alt="">
						<span class="count bg-success"></span>
					</div>
					<div class="profile-name">
						<h5 class="mb-0 font-weight-normal"><?= $this->session->userdata('nama_admin'); ?></h5>
						<span>
							<?php
							if ($this->session->userdata('role') == 1) {
								echo "Super Admin";
							} else if ($this->session->userdata('role') == 2) {
								echo "Pengurus";
							} else {
								echo "Tidak Terdaftar";
							}
							?>
						</span>
					</div>
				</div>
				<a href="#" id="profile-dropdown" data-toggle="dropdown"><i class="mdi mdi-dots-vertical"></i></a>
				<div class="dropdown-menu dropdown-menu-right sidebar-dropdown preview-list" aria-labelledby="profile-dropdown">
					<a href="#" class="dropdown-item preview-item">
						<div class="preview-thumbnail">
							<div class="preview-icon bg-dark rounded-circle">
								<i class="mdi mdi-settings text-primary"></i>
							</div>
						</div>
						<div class="preview-item-content">
							<p class="preview-subject ellipsis mb-1 text-small">Account settings</p>
						</div>
					</a>
					<div class="dropdown-divider"></div>
					<a href="#" class="dropdown-item preview-item">
						<div class="preview-thumbnail">
							<div class="preview-icon bg-dark rounded-circle">
								<i class="mdi mdi-onepassword  text-info"></i>
							</div>
						</div>
						<div class="preview-item-content">
							<p class="preview-subject ellipsis mb-1 text-small">Change Password</p>
						</div>
					</a>
				</div>
			</div>
		</li>
		<li class="nav-item nav-category">
			<span class="nav-link">Navigation</span>
		</li>
		<?php
		if ($this->session->userdata('role') == 1) {
		?>
			<li class="nav-item menu-items">
				<a class="nav-link" href="<?= base_url(); ?>admin/beranda">
					<span class="menu-icon">
						<i class="mdi mdi-speedometer"></i>
					</span>
					<span class="menu-title">Dashboard</span>
				</a>
			</li>

			<li class="nav-item menu-items">
				<a class="nav-link" href="<?= base_url(); ?>admin/pengurus">
					<span class="menu-icon">
						<i class="mdi mdi-account-check"></i>
					</span>
					<span class="menu-title">Daftar Pengurus</span>
				</a>
			</li>

			<li class="nav-item menu-items">
				<a class="nav-link" href="<?= base_url(); ?>admin/device">
					<span class="menu-icon">
						<i class="mdi mdi-cast-connected"></i>
					</span>
					<span class="menu-title">Daftar Device</span>
				</a>
			</li>

			<li class="nav-item menu-items">
				<a class="nav-link" href="<?= base_url(); ?>admin/rfidnew">
					<span class="menu-icon">
						<i class="mdi mdi-credit-card-multiple"></i>
					</span>
					<span class="menu-title">Kartu RFID Baru</span>
				</a>
			</li>

			<li class="nav-item menu-items">
				<a class="nav-link" href="<?= base_url(); ?>admin/listrfid">
					<span class="menu-icon">
						<i class="mdi mdi-account-card-details"></i>
					</span>
					<span class="menu-title">Daftar User RFID</span>
				</a>
			</li>

			<li class="nav-item menu-items">
				<a class="nav-link" href="<?= base_url(); ?>admin/absensi_user">
					<span class="menu-icon">
						<i class="mdi mdi-book-open-variant"></i>
					</span>
					<span class="menu-title">Data Absensi</span>
				</a>
			</li>

			<li class="nav-item menu-items">
				<a class="nav-link" href="<?= base_url(); ?>admin/history">
					<span class="menu-icon">
						<i class="mdi mdi-history"></i>
					</span>
					<span class="menu-title">History Device</span>
				</a>
			</li>

			<li class="nav-item menu-items">
				<a class="nav-link" href="<?= base_url(); ?>admin/setting">
					<span class="menu-icon">
						<i class="mdi mdi-settings"></i>
					</span>
					<span class="menu-title">Setting</span>
				</a>
			</li>
		<?php
		} else if ($this->session->userdata('role') == 2) {
		?>
			<li class="nav-item menu-items">
				<a class="nav-link" href="<?= base_url(); ?>admin/beranda">
					<span class="menu-icon">
						<i class="mdi mdi-speedometer"></i>
					</span>
					<span class="menu-title">Dashboard</span>
				</a>
			</li>

		<?php
		}
		?>
		<li class="nav-item menu-items">
			<a class="nav-link" href="http://www.bootstrapdash.com/demo/corona-free/jquery/documentation/documentation.html">
				<span class="menu-icon">
					<i class="mdi mdi-file-document-box"></i>
				</span>
				<span class="menu-title">Dokumenstasi</span>
			</a>
		</li>
	</ul>
</nav>