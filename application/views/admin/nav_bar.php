<?php
?>
<nav class="navbar p-0 fixed-top d-flex flex-row">
	<div class="navbar-brand-wrapper d-flex d-lg-none align-items-center justify-content-center">
		<a class="navbar-brand brand-logo-mini" href="<?= base_url(); ?>beranda"><img src="<?= base_url(); ?>assets/images/logo.png" alt="logo" /></a>
	</div>
	<div class="navbar-menu-wrapper flex-grow d-flex align-items-stretch">
		<button class="navbar-toggler navbar-toggler align-self-center" type="button" data-toggle="minimize">
			<span class="mdi mdi-menu"></span>
		</button>

		<ul class="navbar-nav navbar-nav-right">
			<li class="nav-item dropdown">
				<a class="nav-link count-indicator dropdown-toggle" id="notificationDropdown" href="#" data-toggle="dropdown">
					<i class="mdi mdi-bell"></i>
					<?php
					if ($this->M_admin->get_rfid_new_count() > 0) {
					?>
						<span class="count bg-danger"></span>
					<?php
					} else {
					?>
						<span class="count"></span>
					<?php
					}
					?>
				</a>
				<div class="dropdown-menu dropdown-menu-right navbar-dropdown preview-list" aria-labelledby="notificationDropdown">
					<h6 class="p-3 mb-0">Notifikasi</h6>
					<?php
					if ($this->M_admin->get_rfid_new_count() > 0) {
					?>
						<div class="dropdown-divider"></div>
						<a class="dropdown-item preview-item" href="<?= base_url(); ?>admin/rfidnew">
							<div class="preview-thumbnail">
								<div class="preview-icon bg-dark rounded-circle">
									<i class="mdi mdi-account-card-details text-success"></i>
								</div>
							</div>
							<div class="preview-item-content">
								<p class="preview-subject mb-1">Kartu RFID</p>
								<p class="text-success ellipsis mb-0"> Ada 1 kartu RFID baru </p>
							</div>
						</a>
					<?php
					} else {
					?>
						<div class="dropdown-divider"></div>
						<p class="p-3 mb-0 text-center text-danger">Tidak Ada Notifikasi</p>
					<?php
					}
					?>
					<div class="dropdown-divider"></div>
				</div>
			</li>
			<li class="nav-item dropdown">
				<a class="nav-link" id="profileDropdown" href="#" data-toggle="dropdown">
					<div class="navbar-profile">
						<img class="img-xs rounded-circle" src="<?= base_url(); ?>assets/images/admin/<?= $this->session->userdata('avatar'); ?>" alt="">
						<p class="mb-0 d-none d-sm-block navbar-profile-name"><?= $this->session->userdata('nama_admin'); ?></p>
						<i class="mdi mdi-menu-down d-none d-sm-block"></i>
					</div>
				</a>
				<div class="dropdown-menu dropdown-menu-right navbar-dropdown preview-list" aria-labelledby="profileDropdown">
					<h6 class="p-3 mb-0">Profile</h6>
					<div class="dropdown-divider"></div>
					<a class="dropdown-item preview-item">
						<div class="preview-thumbnail">
							<div class="preview-icon bg-dark rounded-circle">
								<i class="mdi mdi-settings text-success"></i>
							</div>
						</div>
						<div class="preview-item-content">
							<p class="preview-subject mb-1">Settings</p>
						</div>
					</a>
					<div class="dropdown-divider"></div>
					<a href="<?= base_url(); ?>login/logout" class="dropdown-item preview-item">
						<div class="preview-thumbnail">
							<div class="preview-icon bg-dark rounded-circle">
								<i class="mdi mdi-logout text-danger"></i>
							</div>
						</div>
						<div class="preview-item-content">
							<p class="preview-subject mb-1">Log out</p>
						</div>
					</a>
					<div class="dropdown-divider"></div>
				</div>
			</li>
		</ul>
		<button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button" data-toggle="offcanvas">
			<span class="mdi mdi-format-line-spacing"></span>
		</button>
	</div>
</nav>