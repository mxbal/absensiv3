<?php
class M_admin extends CI_Model
{

    function get_pengurus()
    {
        $this->db->select('*');
        $this->db->from('admin');
        $this->db->where('role', 2);
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result();
        }
    }

    public function create($table, $data)
    {
        $this->db->insert($table, $data);
    }

    public function find($table, $where)
    {
        return $this->db->get_where($table, $where)->row();
    }

    public function update($table, $data, $where)
    {
        $this->db->update($table, $data, $where);
    }

    public function delete($table, $where)
    {
        $this->db->delete($table, $where);
    }

    public function gethistory()
    {
        return $this->db->query("SELECT * FROM history JOIN device ON history.id_device=device.id_device ORDER BY id_history DESC LIMIT 1000 ")->result();
    }

    public function getwaktumasuk()
    {
        return $this->db->query("SELECT * FROM waktu_operasional WHERE ket = 'masuk'")->row();
    }

    public function getwaktukeluar()
    {
        return $this->db->query("SELECT * FROM waktu_operasional WHERE ket = 'keluar'")->row();
    }

    public function updatemasuk($data, $where)
    {
        $this->db->update('waktu_operasional', $data, $where);
    }

    public function updatekeluar($data, $where)
    {
        $this->db->update('waktu_operasional', $data, $where);
    }

    function get_device()
    {
        $this->db->select('*');
        $this->db->from('device');
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result();
        }
    }

    function get_device_id($id)
    {
        $this->db->where('id_device', $id);
        $query = $this->db->get('device');

        if ($query->num_rows() > 0) {
            return $query->result();
        }
    }

    function del_device($id)
    {
        $this->db->where('id_device', $id);
        $this->db->delete('device');
        if ($this->db->affected_rows() == 1) {
            return TRUE;
        }
        return FALSE;
    }

    function get_rfid()
    {
        $this->db->select('*');
        $this->db->from('rfid_user');
        $this->db->order_by('id_rfid', 'desc');
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result();
        }
    }

    function get_rfid_new()
    {
        $this->db->select('*');
        $this->db->from('rfid_user');
        $this->db->where('nama', NULL);
        $this->db->order_by('id_rfid', 'desc');
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result();
        }
    }

    function get_rfid_new_count()
    {
        $this->db->select('*');
        $this->db->from('rfid_user');
        $this->db->where('nama', NULL);
        $this->db->order_by('id_rfid', 'desc');
        $query = $this->db->get();

        return $query->num_rows();
    }

    function get_rfid_byid($id)
    {
        $this->db->where('id_rfid', $id);
        $query = $this->db->get('rfid_user');

        if ($query->num_rows() > 0) {
            return $query->result();
        }
    }

    function updateRFID($data, $id)
    {
        $this->db->where('id_rfid', $id);
        $this->db->update('rfid_user', $data);

        return ($this->db->affected_rows() > 0);
    }

    function del_rfid($id)
    {
        $this->db->where('id_rfid', $id);
        $this->db->delete('rfid_user');
        if ($this->db->affected_rows() == 1) {
            return TRUE;
        }
        return FALSE;
    }

    function get_absensi($today, $tomorrow)
    {
        $this->db->select('*');
        $this->db->from('absensi');
        $this->db->join('device', 'absensi.id_device=device.id_device', 'inner');
        $this->db->join('rfid_user', 'absensi.id_rfid=rfid_user.id_rfid', 'inner');

        $this->db->where("waktu_masuk >=", $today);
        $this->db->where("waktu_masuk <", $tomorrow);
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result();
        }
    }

    function count_rfid()
    {
        $this->db->from('rfid_user');

        return $this->db->count_all_results();
    }

    function count_device()
    {
        $this->db->from('device');

        return $this->db->count_all_results();
    }

    function count_pengurus()
    {
        $this->db->from('admin');
        $this->db->where('role', 2);

        return $this->db->count_all_results();
    }
}
