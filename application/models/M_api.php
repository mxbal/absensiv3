<?php
date_default_timezone_set('Asia/Jakarta');

class M_api extends CI_Model
{
    public function getkey()
    {
        return $this->db->get_where('secret_key', ['id' => 1])->row();
    }

    public function getmodedevice($iddev)
    {
        return $this->db->get_where('device', ['id_device' => $iddev])->row();
    }

    public function getuid($uid)
    {
        return $this->db->get_where('rfid_user', ['uid' => $uid])->row();
    }

    public function createuid($data)
    {
        $this->db->insert('rfid_user', $data);
    }

    public function createhistory($data)
    {
        $this->db->insert('history', $data);
    }

    public function getwaktumasuk()
    {
        return $this->db->get_where('waktu_operasional', ['ket' => 'masuk'])->row();
    }

    public function getwaktukeluar()
    {
        return $this->db->get_where('waktu_operasional', ['ket' => 'keluar'])->row();
    }

    public function checkabsen($uid)
    {
        $date = date('Y-m-d');

        return $this->db->query("SELECT * FROM absensi WHERE id_rfid ='$uid' AND tanggal='$date' ORDER BY id_absensi DESC LIMIT 1 ")->row();
    }

    public function createabsensi($data)
    {
        $this->db->insert('absensi', $data);
    }

    public function updateabsensi($id, $data)
    {
        $this->db->update('absensi', $data, ['id_absensi' => $id]);
    }
}
