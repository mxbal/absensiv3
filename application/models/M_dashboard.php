<?php
class M_dashboard extends CI_Model {
        
    function get_device(){
        $this->db->select('*');
        $this->db->from('device');
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result();
        }
    }

    function get_device_id($id){
        $query = $this->db->where('id_device',$id);
        $q = $this->db->get('device');
        $data = $q->result();
        
        return $data;
    }

}

?>