<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Device extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('M_admin');
		date_default_timezone_set("asia/jakarta");
	}

	public function index()
	{
		if ($this->session->userdata('role') == 1) {
			$data['device'] = $this->M_admin->get_device();
			$this->load->view('admin/v_device', $data);
		} else {
			if ($this->session->userdata('role')) {
				$this->session->set_flashdata("pesan", "<div class=\"alert alert-danger text-center\" id=\"alert\"><i class=\"glyphicon glyphicon-remove\"></i> Tidak bisa akses, Area khusus Super Admin</div>");
				redirect(base_url() . 'beranda');
			} else {
				$this->session->set_flashdata("pesan", "<div class=\"alert alert-danger text-center\" id=\"alert\"><i class=\"glyphicon glyphicon-remove\"></i> Mohon Login terlebih dahulu</div>");
				redirect(base_url() . 'login');
			}
		}
	}

	public function store()
	{
		login();

		$validate = [
			['field' => 'nama', 'label' => 'Nama', 'rules' => 'required'],
			['field' => 'mode', 'label' => 'Mode', 'rules' => 'required'],
		];

		$this->form_validation->set_rules($validate);

		if ($this->form_validation->run() == true) {
			$data = [
				'nama_device' => $this->input->post('nama', true),
				'mode' => $this->input->post('mode', true),
			];

			$this->M_admin->create('device', $data);

			$response = [
				'status' => 'success',
				'message' => 'Data device berhasil ditambahkan'
			];

			echo json_encode($response);
		} else {
			$response = [
				'status' => 'failed',
				'nama' => form_error('nama'),
				'mode' => form_error('mode'),
			];

			echo json_encode($response);
		}
	}

	public function find($id)
	{
		login();

		$device = $this->M_admin->find('device', ['id_device' => $id]);

		echo json_encode($device);
	}

	public function update($id)
	{
		login();

		$validate = [
			['field' => 'mode', 'label' => 'mode', 'rules' => 'required'],
			['field' => 'nama', 'label' => 'Nama', 'rules' => 'required'],
		];

		$this->form_validation->set_rules($validate);

		if ($this->form_validation->run() == true) {
			$device = $this->M_admin->find('device', ['id_device' => $id]);

			$data = [
				'mode' => $this->input->post('mode', true),
				'nama_device' => $this->input->post('nama', true),
			];

			$this->M_admin->update('device', $data, ['id_device' => $id]);

			$response = [
				'status' => 'success',
				'message' => 'Data device berhasil diupdate'
			];

			echo json_encode($response);
		} else {
			$response = [
				'status' => 'failed',
				'username' => form_error('username'),
				'nama' => form_error('nama'),
			];

			echo json_encode($response);
		}
	}

	public function destroy($id)
	{
		login();

		$this->db->delete('device', ['id_device' => $id]);

		echo json_encode([
			'message' => 'Data device berhasil didelete'
		]);
	}
}
