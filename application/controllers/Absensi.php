<?php
defined('BASEPATH') or exit('No direct script access allowed');

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Absensi extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('M_admin');
		date_default_timezone_set("asia/jakarta");
	}

	public function index()
	{
		if ($this->session->userdata('role') == 1) {
			$data['flag'] = "absensi";

			if ($this->input->get('from') && $this->input->get('to')) {
				$from = strtotime($this->input->get('from'));
				$to = strtotime($this->input->get('to')) + 86400;

				$data['absensi'] = $this->M_admin->get_absensi($from, $to);
			} else {
				$today = strtotime("today");
				$tomorrow = strtotime("tomorrow");

				$data['absensi'] = $this->M_admin->get_absensi($today, $tomorrow);
			}

			$this->load->view('admin/v_absensi', $data);
		} else {
			if ($this->session->userdata('role')) {
				$this->session->set_flashdata("pesan", "<div class=\"alert alert-danger text-center\" id=\"alert\"><i class=\"glyphicon glyphicon-remove\"></i> Tidak bisa akses, Area khusus Super Admin</div>");
				redirect(base_url() . 'beranda');
			} else {
				$this->session->set_flashdata("pesan", "<div class=\"alert alert-danger text-center\" id=\"alert\"><i class=\"glyphicon glyphicon-remove\"></i> Mohon Login terlebih dahulu</div>");
				redirect(base_url() . 'login');
			}
		}
	}

	public function last_absensi()
	{
		if ($this->session->userdata('role')) {
			if (isset($_POST['tanggal'])) {
				$tgl = $this->input->post('tanggal');
				//echo $tgl;
				$split1 = explode("-", $tgl);
				$x = 0;
				foreach ($split1 as $key => $value) {
					$date[$x] = $value;
					$x++;
				}

				$ts1 = strtotime($date[0]);
				$ts2 = strtotime($date[1]);

				$tgl1 = date("d-M-Y", $ts1);
				$tgl2 = date("d-M-Y", $ts2);

				$ts2 += 86400;	// tambah 1 hari (hitungan detik)

				// $data['tgl1'] = $tgl1;
				// $data['tgl2'] = $tgl2;

				if ($x == 2) {
					$data['datamasuk'] = $this->M_admin->get_absensi($ts1, $ts2);
					$data['tanggal'] = $tgl1 . " - " . $tgl2;

					if ($tgl1 == $tgl2) {
						$data['waktuabsensi'] = $tgl1;
					} else {
						$data['waktuabsensi'] = $tgl1 . " - " . $tgl2;
					}

					$data['flag'] = "last-absensi";
					$this->load->view('admin/v_absensi', $data);
				} else {
					redirect(base_url() . 'admin/absensi_user');
				}
			} else {
				redirect(base_url() . 'admin/absensi_user');
			}
		}
	}

	public function export()
	{
		login();

		if ($_GET['from'] != '' && $_GET['to'] != '') {
			$from = strtotime($this->input->get('from'));
			$to = strtotime($this->input->get('to')) + 86400;

			$absensi = $this->M_admin->get_absensi($from, $to);
		} else {
			$from = strtotime("today");
			$to = strtotime("tomorrow");

			$absensi = $this->M_admin->get_absensi($from, $to);
		}

		$spreadsheet = new Spreadsheet;

		$spreadsheet->setActiveSheetIndex(0)
			->setCellValue('A1', 'No')
			->setCellValue('B1', 'Device')
			->setCellValue('C1', 'Nama')
			->setCellValue('D1', 'Jabatan/Kelas')
			->setCellValue('E1', 'Waktu Masuk')
			->setCellValue('F1', 'Waktu Keluar')
			->setCellValue('G1', 'Keterangan');

		$baris = 2;
		$nomor = 1;

		if (isset($absensi)) {
			foreach ($absensi as $data) {
				$spreadsheet->setActiveSheetIndex(0)
					->setCellValue('A' . $baris, $nomor)
					->setCellValue('B' . $baris, $data->nama_device)
					->setCellValue('C' . $baris, $data->nama)
					->setCellValue('D' . $baris, $data->jabatan)
					->setCellValue('E' . $baris, date('d/m/Y H:i:s', $data->waktu_masuk))
					->setCellValue('F' . $baris, date('d/m/Y H:i:s', $data->waktu_keluar))
					->setCellValue('G' . $baris, $data->keterangan);

				$baris++;
				$nomor++;
			}
		}

		$writer = new Xlsx($spreadsheet);
		$title = 'Absensi_Tanggal_' . date('d/m/Y', $from) . '_' . date('d/m/Y', $to) . ".xlsx";

		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename=' . $title);
		header('Cache-Control: max-age=0');

		$writer->save('php://output');
	}
}
