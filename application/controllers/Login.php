<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Login extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('M_login');
		$this->load->library('bcrypt');
	}

	public function index()
	{
		$this->load->view('front/v_login');
	}

	public function check()
	{
		if (isset($_POST['username']) && isset($_POST['password'])) {
			$username = $this->input->post('username');
			$password = $this->input->post('password');

			if (isset($_POST['remember'])) {
				$remember = $this->input->post('remember');
				$hour = time() + 3600 * 24 * 30;
				setcookie('username', $username, $hour);
				setcookie('password', $password, $hour);
			}

			$check = $this->M_login->prosesLogin($username);
			$hasil = 0;
			if (isset($check)) {
				foreach ($check as $key => $value) {
					$hasil++;
				}
			}

			if ($hasil > 0) {
				$data = $this->M_login->viewDataByUsername($username);
				$passDB = "";
				foreach ($data as $dkey) {
					$passDB = $dkey->password;
					$role = $dkey->role;
					$avatar = $dkey->avatar;
					$id_admin = $dkey->id_admin;
					$nama = $dkey->nama;
				}

				if ($this->bcrypt->check_password($password, $passDB)) {
					// Password match
					$this->session->set_userdata('login', true);
					$this->session->set_userdata('id_admin', $id_admin);
					$this->session->set_userdata('nama_admin', $nama);
					$this->session->set_userdata('username', $username);
					$this->session->set_userdata('avatar', $avatar);
					$this->session->set_userdata('role', $role);
					$this->session->set_userdata('first_login', 0);

					redirect(base_url() . 'admin/beranda');
				} else {
					// Password does not match
					$this->session->set_flashdata("pesan", "<div class=\"alert alert-danger text-center\" id=\"alert\"><i class=\"glyphicon glyphicon-remove\"></i> Gagal Login, username atau password salah</div>");
					redirect(base_url() . 'login');
				}
			} else {
				$this->session->set_flashdata("pesan", "<div class=\"alert alert-danger text-center\" id=\"alert\"><i class=\"glyphicon glyphicon-remove\"></i> Gagal Login, username atau password salah</div>");
				redirect(base_url() . 'login');
			}
		} else {
			$this->session->set_flashdata("pesan", "<div class=\"alert alert-danger text-center\" id=\"alert\"><i class=\"glyphicon glyphicon-remove\"></i> Gagal Login</div>");
			redirect(base_url() . 'login');
		}
	}

	public function logout()
	{
		$this->session->sess_destroy();
		redirect(base_url() . 'login');
	}
}
