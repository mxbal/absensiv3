<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Rfid extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('M_admin');
		date_default_timezone_set("asia/jakarta");
	}

	public function index()
	{
		if ($this->session->userdata('role') == 1) {
			$data['flag'] = "rfid";
			$data['rfid'] = $this->M_admin->get_rfid();
			$this->load->view('admin/v_rfid', $data);
		} else {
			if ($this->session->userdata('role')) {
				$this->session->set_flashdata("pesan", "<div class=\"alert alert-danger text-center\" id=\"alert\"><i class=\"glyphicon glyphicon-remove\"></i> Tidak bisa akses, Area khusus Super Admin</div>");
				redirect(base_url() . 'beranda');
			} else {
				$this->session->set_flashdata("pesan", "<div class=\"alert alert-danger text-center\" id=\"alert\"><i class=\"glyphicon glyphicon-remove\"></i> Mohon Login terlebih dahulu</div>");
				redirect(base_url() . 'login');
			}
		}
	}

	public function new()
	{
		if ($this->session->userdata('role') == 1) {
			$data['flag'] = "new";
			$data['rfid'] = $this->M_admin->get_rfid_new();
			$this->load->view('admin/v_rfid', $data);
		} else {
			if ($this->session->userdata('role')) {
				$this->session->set_flashdata("pesan", "<div class=\"alert alert-danger text-center\" id=\"alert\"><i class=\"glyphicon glyphicon-remove\"></i> Tidak bisa akses, Area khusus Super Admin</div>");
				redirect(base_url() . 'beranda');
			} else {
				$this->session->set_flashdata("pesan", "<div class=\"alert alert-danger text-center\" id=\"alert\"><i class=\"glyphicon glyphicon-remove\"></i> Mohon Login terlebih dahulu</div>");
				redirect(base_url() . 'login');
			}
		}
	}

	public function edit($id = null)
	{
		if ($this->session->userdata('role') == 1) {
			if (isset($id)) {
				$data['flag'] = "edit";
				$data['rfid'] = $this->M_admin->get_rfid_byid($id);
				if (isset($data['rfid'])) {
					$this->load->view('admin/v_rfid', $data);
				} else {
					redirect(base_url() . 'beranda');
				}
			}
		} else {
			if ($this->session->userdata('role')) {
				$this->session->set_flashdata("pesan", "<div class=\"alert alert-danger text-center\" id=\"alert\"><i class=\"glyphicon glyphicon-remove\"></i> Tidak bisa akses, Area khusus Super Admin</div>");
				redirect(base_url() . 'beranda');
			} else {
				$this->session->set_flashdata("pesan", "<div class=\"alert alert-danger text-center\" id=\"alert\"><i class=\"glyphicon glyphicon-remove\"></i> Mohon Login terlebih dahulu</div>");
				redirect(base_url() . 'login');
			}
		}
	}

	public function save_edit_rfid()
	{
		if ($this->session->userdata('role') == 1) {
			if (isset($_POST['id_rfid'])) {
				$id_rfid = $this->input->post('id_rfid');
				$nama = $this->input->post('nama');
				$telp = $this->input->post('telp');
				$gender = $this->input->post('gender');
				$jabatan = $this->input->post('jabatan');
				$alamat = $this->input->post('alamat');

				$data = array(
					'nama' => $nama, 'telp' => $telp, 'gender' => $gender,
					'jabatan' => $jabatan, 'alamat' => $alamat
				);

				if ($this->M_admin->updateRFID($data, $id_rfid)) {
					$this->session->set_flashdata("pesan", "<div class=\"alert alert-success text-center\" id=\"success\"><i class=\"glyphicon glyphicon-checklist\"></i> Berhasil Update Data</div>");
				} else {
					$this->session->set_flashdata("pesan", "<div class=\"alert alert-danger text-center\" id=\"alert\"><i class=\"glyphicon glyphicon-remove\"></i> Gagal Update Data</div>");
				}

				redirect(base_url() . 'admin/listrfid');
			} else {
				redirect(base_url() . 'admin/listrfid');
			}
		} else {
			if ($this->session->userdata('role')) {
				$this->session->set_flashdata("pesan", "<div class=\"alert alert-danger text-center\" id=\"alert\"><i class=\"glyphicon glyphicon-remove\"></i> Tidak bisa akses, Area khusus Super Admin</div>");
				redirect(base_url() . 'beranda');
			} else {
				$this->session->set_flashdata("pesan", "<div class=\"alert alert-danger text-center\" id=\"alert\"><i class=\"glyphicon glyphicon-remove\"></i> Mohon Login terlebih dahulu</div>");
				redirect(base_url() . 'login');
			}
		}
	}

	public function hapus_rfid($id = null)
	{
		if ($this->session->userdata('role') == 1) {
			if (isset($id)) {

				if ($this->M_admin->del_rfid($id)) {
					$this->session->set_flashdata("pesan", "<div class=\"alert alert-success text-center\" id=\"success\"><i class=\"glyphicon glyphicon-checklist\"></i> Berhasil Hapus Data</div>");
				} else {
					$this->session->set_flashdata("pesan", "<div class=\"alert alert-danger text-center\" id=\"alert\"><i class=\"glyphicon glyphicon-remove\"></i> Gagal Hapus Data</div>");
				}
				redirect(base_url() . 'admin/listrfid');
			}
		} else {
			if ($this->session->userdata('role')) {
				$this->session->set_flashdata("pesan", "<div class=\"alert alert-danger text-center\" id=\"alert\"><i class=\"glyphicon glyphicon-remove\"></i> Tidak bisa akses, Area khusus Super Admin</div>");
				redirect(base_url() . 'beranda');
			} else {
				$this->session->set_flashdata("pesan", "<div class=\"alert alert-danger text-center\" id=\"alert\"><i class=\"glyphicon glyphicon-remove\"></i> Mohon Login terlebih dahulu</div>");
				redirect(base_url() . 'login');
			}
		}
	}
}
