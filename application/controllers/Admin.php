<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Admin extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('M_admin');
		$this->load->helper('auth_helper');
		date_default_timezone_set("asia/jakarta");
	}

	public function index()
	{
		redirect(base_url() . 'admin/beranda');
	}

	public function x()
	{
		$data = $this->M_admin->count_rfid();

		echo $data;
	}

	public function history()
	{
		login();

		$data = [
			'histories' => $this->M_admin->gethistory(),
			'title' => 'History Device'
		];

		$this->load->view('admin/v_history', $data);
	}
}
