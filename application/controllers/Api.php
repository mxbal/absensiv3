<?php

class Api extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_api');
        date_default_timezone_set('Asia/Jakarta');
    }

    public function index()
    {
        $response = [
            'status' => 'success',
            'message' => 'Api Absensi RFID V3'
        ];

        echo json_encode($response);
    }

    public function getmodejson()
    {
        if (isset($_GET['key']) && isset($_GET['iddev'])) {
            $secretkey = $this->M_api->getkey();

            if ($_GET['key'] == $secretkey->secretkey) {
                $device = $this->M_api->getmodedevice($_GET['iddev']);

                if (isset($device)) {
                    $response = [
                        'status' => 'success',
                        'message' => 'Mode device didapatkan',
                        'mode' => $device->mode,
                    ];
                } else {
                    $response = [
                        'status' => 'failed',
                        'message' => 'Device tidak ditemukan',
                    ];
                }
            } else {
                $response = [
                    'status' => 'failed',
                    'message' => 'Salah secret key'
                ];
            }
        } else {
            $response = [
                'status' => 'failed',
                'message' => 'Salah parameter'
            ];
        }

        echo json_encode($response);
    }

    public function addcardjson()
    {
        if (isset($_GET['key']) && isset($_GET['iddev']) && isset($_GET['uid'])) {
            $secretkey = $this->M_api->getkey();

            if ($_GET['key'] == $secretkey->secretkey) {
                $uid = $this->M_api->getuid($_GET['uid']);
                $device = $this->M_api->getmodedevice($_GET['iddev']);

                if (isset($uid)) {
                    $response = [
                        'status' => 'failed',
                        'message' => 'Uid sudah terdaftar',
                        'uid' => $uid->uid
                    ];
                } else {
                    if (isset($device)) {
                        $data = [
                            'uid' => $_GET['uid']
                        ];

                        $this->M_api->createuid($data);

                        $datahistory = [
                            'id_device' => $device->id_device,
                            'ket' => 'Add rfid card',
                            'waktu' => date('Y-m-d H:i:s')
                        ];

                        $this->M_api->createhistory($datahistory);

                        $response = [
                            'status' => 'suucess',
                            'message' => 'Uid berhasil terdaftar',
                            'uid' => $_GET['uid']
                        ];
                    }
                }
            } else {
                $response = [
                    'status' => 'failed',
                    'message' => 'Salah secret key',
                ];
            }
        } else {
            $response = [
                'status' => 'failed',
                'message' => 'Salah parameter',
            ];
        }

        echo json_encode($response);
    }

    public function absensijson()
    {
        if (isset($_GET['key']) && isset($_GET['iddev']) && $_GET['uid']) {
            $secretkey = $this->M_api->getkey();

            if ($_GET['key'] == $secretkey->secretkey) {
                $device = $this->M_api->getmodedevice($_GET['iddev']);

                if (isset($device)) {
                    $uid = $this->M_api->getuid($_GET['uid']);

                    if (isset($uid)) {
                        $masuk = $this->M_api->getwaktumasuk()->waktu;
                        $keluar = $this->M_api->getwaktukeluar()->waktu;
                        $now = strtotime(date('H:i:s'));

                        $masukawal = strtotime(explode('-', $masuk)[0]);
                        $masukakhir = strtotime(explode('-', $masuk)[1]);
                        $keluarawal = strtotime(explode('-', $keluar)[0]);
                        $keluarakhir = strtotime(explode('-', $keluar)[1]);

                        if ($now < $masukawal && $now > $keluarakhir) {
                            $response = [
                                'status' => 'failed',
                                'message' => 'Absensi diluar waktu operasional'
                            ];
                        }

                        if ($now >= $masukawal && $now <= $masukakhir) {
                            $cekabsen = $this->M_api->checkabsen($uid->id_rfid);

                            if ($cekabsen == null) {
                                $data = [
                                    'id_device' => $device->id_device,
                                    'id_rfid' => $uid->id_rfid,
                                    'tanggal' => date('Y-m-d'),
                                    'waktu_masuk' => strtotime(date('Y-m-d H:i:s')),
                                    'waktu_keluar' => 0,
                                    'ket' => 'Masuk'
                                ];

                                $this->M_api->createabsensi($data);

                                $datahistory = [
                                    'id_device' => $device->id_device,
                                    'ket' => 'Insert Absensi Masuk',
                                    'waktu' => date('Y-m-d H:i:s')
                                ];

                                $this->M_api->createhistory($datahistory);

                                $response = [
                                    'status' => 'success',
                                    'message' => 'Absensi masuk berhasil'
                                ];
                            } else {
                                $response = [
                                    'status' => 'failed',
                                    'message' => 'Anda sudah melakukan absensi masuk'
                                ];
                            }
                        }

                        if ($now > $masukakhir && $now >= $keluarawal && $now <= $keluarakhir) {
                            $cekabsen = $this->M_api->checkabsen($uid->id_rfid);

                            if (isset($cekabsen) && $cekabsen->waktu_keluar == 0) {
                                $data = [
                                    'waktu_keluar' => strtotime(date('Y-m-d H:i:s')),
                                ];

                                $this->M_api->updateabsensi($cekabsen->id_absensi, $data);

                                $datahistory = [
                                    'id_device' => $device->id_device,
                                    'ket' => 'Insert Absensi Keluar',
                                    'waktu' => date('Y-m-d H:i:s')
                                ];

                                $this->M_api->createhistory($datahistory);

                                $response = [
                                    'status' => 'success',
                                    'message' => 'Absensi keluar berhasil'
                                ];
                            } else if ($cekabsen == null) {
                                $response = [
                                    'status' => 'failed',
                                    'message' => 'Anda belum melakukan absensi masuk'
                                ];
                            } else {
                                $response = [
                                    'status' => 'failed',
                                    'message' => 'Anda sudah melakukan absensi keluar'
                                ];
                            }
                        }

                        if ($now > $keluarakhir) {
                            $response = [
                                'status' => 'failed',
                                'message' => 'Absensi diluar waktu operasional'
                            ];
                        }
                    } else {
                        $response = [
                            'status' => 'failed',
                            'message' => 'Uid tidak ditemukan'
                        ];
                    }
                } else {
                    $response = [
                        'status' => 'failed',
                        'message' => 'Device tidak ditemukan'
                    ];
                }
            } else {
                $response = [
                    'status' => 'failed',
                    'message' => 'Salah secret key'
                ];
            }
        } else {
            $response = [
                'status' => 'failed',
                'message' => 'Salah parameter'
            ];
        }

        echo json_encode($response);
    }
}
