<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Dashboard extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('M_dashboard');
		date_default_timezone_set("asia/jakarta");
	}

	public function index()
	{
		$data['flag'] = "pilih-device";
		$data['device'] = $this->M_dashboard->get_device();
		$this->load->view('front/v_dashboard', $data);
	}

	public function device($id = null)
	{
		if (isset($id)) {
			$data['flag'] = "-";
			$data['device'] = $this->M_dashboard->get_device_id($id);
			$this->load->view('front/v_dashboard', $data);
		} else {
			redirect(base_url() . 'dashboard');
		}
	}
}
