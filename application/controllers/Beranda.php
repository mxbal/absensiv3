<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Beranda extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('M_admin');
		date_default_timezone_set("asia/jakarta");
	}

	public function index()
	{
		if ($this->session->userdata('role')) {
			$data['rfid'] = $this->M_admin->count_rfid();
			$data['device'] = $this->M_admin->count_device();
			$data['pengurus'] = $this->M_admin->count_pengurus();

			$this->load->view('admin/v_beranda', $data);
		} else {
			$this->session->set_flashdata("pesan", "<div class=\"alert alert-danger text-center\" id=\"alert\"><i class=\"glyphicon glyphicon-remove\"></i> Mohon Login terlebih dahulu</div>");
			redirect(base_url() . 'login');
		}
	}
}
