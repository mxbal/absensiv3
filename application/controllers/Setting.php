<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Setting extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('M_admin');
		date_default_timezone_set("asia/jakarta");
	}

	public function index()
	{
		login();

		if ($this->session->userdata('role') == 1) {
			$masuk = $this->M_admin->find('waktu_operasional', ['ket' => 'masuk']);
			$keluar = $this->M_admin->find('waktu_operasional', ['ket' => 'keluar']);

			$data['awal_masuk'] = explode('-', $masuk->waktu)[0];
			$data['akhir_masuk'] = explode('-', $masuk->waktu)[1];
			$data['awal_keluar'] = explode('-', $keluar->waktu)[0];
			$data['akhir_keluar'] = explode('-', $keluar->waktu)[1];

			$this->load->view('admin/v_setting', $data);
		} else {
			$this->session->set_flashdata("pesan", "<div class=\"alert alert-danger text-center\" id=\"alert\"><i class=\"glyphicon glyphicon-remove\"></i> Mohon Login terlebih dahulu</div>");
			redirect(base_url() . 'login');
		}
	}

	public function update()
	{
		$masuk = $this->input->post('waktu_awal_masuk', true) . '-' . $this->input->post('waktu_akhir_masuk', true);
		$keluar = $this->input->post('waktu_awal_pulang', true) . '-' . $this->input->post('waktu_akhir_pulang', true);

		$this->M_admin->update('waktu_operasional', ['waktu' => $masuk], ['ket' => 'masuk']);
		$this->M_admin->update('waktu_operasional', ['waktu' => $keluar], ['ket' => 'keluar']);

		$this->session->set_flashdata("pesan", "<div class=\"alert alert-success text-center\" id=\"success\"><i class=\"glyphicon glyphicon-checklist\"></i> Berhasil update setting</div>");

		redirect(base_url('admin/setting'));
	}
}
