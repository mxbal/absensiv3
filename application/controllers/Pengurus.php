<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Pengurus extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('M_admin');
		date_default_timezone_set("asia/jakarta");
	}

	public function index()
	{
		if ($this->session->userdata('role') == 1) {
			$data['pengurus'] = $this->M_admin->get_pengurus();
			$this->load->view('admin/v_pengurus', $data);
		} else {
			if ($this->session->userdata('role')) {
				$this->session->set_flashdata("pesan", "<div class=\"alert alert-danger text-center\" id=\"alert\"><i class=\"glyphicon glyphicon-remove\"></i> Tidak bisa akses, Area khusus Super Admin</div>");
				redirect(base_url() . 'beranda');
			} else {
				$this->session->set_flashdata("pesan", "<div class=\"alert alert-danger text-center\" id=\"alert\"><i class=\"glyphicon glyphicon-remove\"></i> Mohon Login terlebih dahulu</div>");
				redirect(base_url() . 'login');
			}
		}
	}

	public function store()
	{
		login();

		$validate = [
			['field' => 'username', 'label' => 'Username', 'rules' => 'required'],
			['field' => 'nama', 'label' => 'Nama', 'rules' => 'required'],
			['field' => 'password', 'label' => 'Password', 'rules' => 'required'],
		];

		$this->form_validation->set_rules($validate);

		if ($this->form_validation->run() == true) {
			$data = [
				'username' => $this->input->post('username', true),
				'nama' => $this->input->post('nama', true),
				'password' => password_hash($this->input->post('password', true), PASSWORD_DEFAULT),
				'role' => 2,
				'avatar' => 'default.png',
				'status' => 1
			];

			$this->M_admin->create('admin', $data);

			$response = [
				'status' => 'success',
				'message' => 'Data pengurus berhasil ditambahkan'
			];

			echo json_encode($response);
		} else {
			$response = [
				'status' => 'failed',
				'username' => form_error('username'),
				'nama' => form_error('nama'),
				'password' => form_error('password'),
			];

			echo json_encode($response);
		}
	}

	public function find($id)
	{
		login();

		$pengurus = $this->M_admin->find('admin', ['id_admin' => $id]);

		echo json_encode($pengurus);
	}

	public function update($id)
	{
		login();

		$validate = [
			['field' => 'username', 'label' => 'Username', 'rules' => 'required'],
			['field' => 'nama', 'label' => 'Nama', 'rules' => 'required'],
		];

		$this->form_validation->set_rules($validate);

		if ($this->form_validation->run() == true) {
			$pengurus = $this->M_admin->find('admin', ['id_admin' => $id]);

			$data = [
				'username' => $this->input->post('username', true),
				'nama' => $this->input->post('nama', true),
				'password' => $this->input->post('password', true) != '' ?  password_hash($this->input->post('password', true), PASSWORD_DEFAULT) : $pengurus->password,
			];

			$this->M_admin->update('admin', $data, ['id_admin' => $id]);

			$response = [
				'status' => 'success',
				'message' => 'Data pengurus berhasil diupdate'
			];

			echo json_encode($response);
		} else {
			$response = [
				'status' => 'failed',
				'username' => form_error('username'),
				'nama' => form_error('nama'),
			];

			echo json_encode($response);
		}
	}

	public function destroy($id)
	{
		login();

		$this->db->delete('admin', ['id_admin' => $id]);

		echo json_encode([
			'message' => 'Data pengurus berhasil didelete'
		]);
	}
}
